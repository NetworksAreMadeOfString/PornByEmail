package main

import (
	"github.com/emersion/go-imap"
	"os"
	//"github.com/emersion/go-imap/client"
	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	//"log"
	"bytes"
	"github.com/scorredoira/email"
	"io/ioutil"
	"log"
	"math/rand"
	"net/mail"
	"time"
	//"net/smtp"
	"strings"
)

func SendSomePorn(msg *imap.Envelope) error {
	SMTPServer := os.Getenv("SMTP_SERVER")
	SMTPUser := os.Getenv("SMTP_USER")
	SMTPPass := os.Getenv("SMTP_PASS")

	auth := sasl.NewPlainClient("", SMTPUser, SMTPPass)

	toAddr := msg.From[0].MailboxName + "@" + msg.From[0].HostName

	m := email.NewMessage("Re: "+msg.Subject, "Attached to this email is some pornography (or lolcats).\r\n\r\n"+
		"We would recommend that you contact the BBFC ( http://bbfc.co.uk/about-bbfc/contact-us ) and complain that this service has no age verification.\r\n\r\n"+
		"You can also send an email with the word \"help\" as the subject to SendMe@PornBy.Email for guidance on how to use this service.")
	m.From = mail.Address{Name: "PornByEmail", Address: "sendme@pornby.email"}
	m.To = []string{toAddr}

	// add attachments
	attachment, attachErr := getAttachment()
	if attachErr != nil {
		log.Println(attachErr)
		attachment = "images/1.jpg"
	}

	if err := m.Attach(attachment); err != nil {
		return err
	}

	r := bytes.NewReader(m.Bytes())
	err := smtp.SendMail(SMTPServer, auth, "sendme@pornby.email", m.To, r)
	if err != nil {
		return err
	}
	return nil
}

func SendSomeHelp(msg *imap.Envelope) error {
	SMTPServer := os.Getenv("SMTP_SERVER")
	SMTPUser := os.Getenv("SMTP_USER")
	SMTPPass := os.Getenv("SMTP_PASS")

	auth := sasl.NewPlainClient("", SMTPUser, SMTPPass)

	toAddr := msg.From[0].MailboxName + "@" + msg.From[0].HostName

	to := []string{toAddr}

	replyMsg := strings.NewReader("To: " + toAddr + "\r\n" +
		"From: PornByEmail <sendme@pornby.email>\r\n" +
		"Subject: How to Use PornBy.email\r\n" +
		"\r\n" +
		"How to Use PornBy.Email:\r\n\r\n" +
		"Sending a message with a subject of \"help\" will result in this reply.\r\n\r\n" +
		"If you send a message with the subject \"preference\" followed by a performers name the software will do a search of the database for any images with meta-data that matches your query e.g. \"preference john smith\"\r\n\r\n" +
		"If you have any other questions you should send a Twitter DM to @PornByEmail\r\n\r\n" +
		"------------------------\r\n\r\n" +
		"Frequently Asked Questions:\r\n\r\n" +
		"1. Is this commercial? Nope\r\n" +
		"2. Why isn't there a website? Just to mess with the BBFC's blocking powers.\r\n" +
		"3. Who is running this? An 'anti internet censorship troublemaker known as Gareth, find him on Twitter; @NetworkString\r\n" +
		"4. Who is sending these emails? No-one, this software is autonomous, no human interaction is required.\r\n" +
		"5. Can I run my own version? Sure, it'll soon be open source and available as a docker image")

	err := smtp.SendMail(SMTPServer, auth, "sendme@pornby.email", to, replyMsg)
	if err != nil {
		return err
	}

	return nil
}

func getAttachment() (string, error) {
	files, err := ioutil.ReadDir("./images/")

	if err != nil {
		return "", err
	}

	rand.Seed(time.Now().Unix())
	return "images/" + files[rand.Intn(len(files))].Name(), nil
}
