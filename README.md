# Porn By Email

This is a proof of concept for delivering material that would fall foul of the
Digital Economy Act 2017 and be liable to blocking by the British Board of 
Film Censors.

Because the primary content mechanism doesn't use a website and the BBFC cannot
force ASPs to block emails it makes sense to deliver said material via email.

## How to use
Send an email with the subject "help" to SendMe@PornBy.Email for instructions
and an FAQ.


## How to add content
Send a DM to the Twitter account @PornByEmail for details


## Future Plans
- A web interface for creating a content publishing account
- Docker images for people to run this software against their own email accounts
