package main

import (
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"log"
	"os"
	"strings"
	"time"
)

func main() {

	//Start the process that checks the mail
	CheckMail()

	log.Println("Done!")
}

func CheckMail() {
	IMAPServer := os.Getenv("IMAP_SERVER")
	IMAPUser := os.Getenv("IMAP_USER")
	IMAPPass := os.Getenv("IMAP_PASS")

	log.Println("Connecting to server...")

	// Connect to server
	c, err := client.DialTLS(IMAPServer, nil)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected")

	// Don't forget to logout
	defer c.Logout()

	// Login
	if err := c.Login(IMAPUser, IMAPPass); err != nil {
		log.Fatal(err)
	}
	log.Println("Logged in")
	// List mailboxes
	/*mailboxes := make(chan *imap.MailboxInfo, 10)
	done = make(chan error, 1)
	go func() {
		done <- c.List("", "*", mailboxes)
	}()*/
	/*log.Println("Mailboxes:")
	for m := range mailboxes {
		log.Println("* " + m.Name)
	}

	if err := <-done; err != nil {
		log.Fatal(err)
	}*/

	for {
		// Select INBOX
		mbox, err := c.Select("INBOX", false)
		if err != nil {
			log.Println(err)
			continue
		}
		log.Println("Flags for INBOX:", mbox.Flags)

		if mbox.Messages > 0 {

			// Get the last 4 messages
			from := uint32(1)
			to := mbox.Messages
			if mbox.Messages > 3 {
				// We're using unsigned integers here, only substract if the result is > 0
				from = mbox.Messages - 3
			}
			seqset := new(imap.SeqSet)
			seqset.AddRange(from, to)

			messages := make(chan *imap.Message, 10)
			done := make(chan error, 1)
			go func() {
				done <- c.Fetch(seqset, []imap.FetchItem{imap.FetchEnvelope}, messages)
			}()

			deleteSeqSet := new(imap.SeqSet)
			log.Println("Last 4 messages:")
			for msg := range messages {

				log.Println("*  " + msg.Envelope.Subject)
				log.Println(" - " + msg.Envelope.ReplyTo[0].MailboxName + "@" + msg.Envelope.ReplyTo[0].HostName)
				log.Println(" - " + msg.Envelope.From[0].MailboxName + "@" + msg.Envelope.From[0].HostName)

				var sentErr error
				subject := strings.ToLower(msg.Envelope.Subject)

				if strings.Contains(subject, "help") {
					sentErr = SendSomeHelp(msg.Envelope)
				} else {
					sentErr = SendSomePorn(msg.Envelope)
				}

				if sentErr == nil {
					log.Printf("Adding %d to the delete queue\n", msg.SeqNum)
					deleteSeqSet.AddNum(msg.SeqNum)
				}

			}

			//Delete the mail
			item := imap.FormatFlagsOp(imap.AddFlags, true)
			flags := []interface{}{imap.DeletedFlag}
			if storeErr := c.Store(deleteSeqSet, item, flags, nil); err != nil {
				log.Println(storeErr)
				continue
			}

			if expungeErr := c.Expunge(nil); err != nil {
				log.Println(expungeErr)
				continue
			}

			if err := <-done; err != nil {
				log.Println(err)
				continue
			}
		}

		log.Println("Sleeping...")

		time.Sleep(1 * time.Minute)
	}
}
